$(function() {
  var navLinks = [
    {
      name: "Home",
      link: "index.html"
    },
    {
      name: "About",
      link: "about.html"
    }
  ];
    var navTemplateScript = $("#build-nav").html();
    var navTemplate = Handlebars.compile (navTemplateScript);
    $(".top-nav").append (navTemplate(navLinks));
});

