// Include gulp
var gulp        = require('gulp');

// Include Our Plugins
var jshint      = require('gulp-jshint');
var sass        = require('gulp-sass');
var sprite      = require('css-sprite').stream;
var concat      = require('gulp-concat');
var uglify      = require('gulp-uglify');
var rename      = require('gulp-rename');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;
var bourbon     = require('node-bourbon');

bourbon.includePaths // Array of Bourbon paths

// Lint Task
gulp.task('lint', function() {
	return gulp.src('app/assets/scripts/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});

gulp.task('sass', function () {
	return gulp.src('app/assets/styles/*.scss')
		.pipe(sass({
			includePaths: require('node-bourbon').includePaths
		}))
		.pipe(gulp.dest('dist/css'))
		.pipe(reload({stream:true}));
});

// generate sprite.png and _sprite.scss
gulp.task('sprites', function () {
	return gulp.src('./src/img/*.png')
		.pipe(sprite({
			name: 'sprite',
			style: '_sprite.scss',
			cssPath: '../images',
			processor: 'scss'
		}))
		.pipe(gulpif('*.png', gulp.dest('../dist/images/'), gulp.dest('./app/assets/styles/modules/')))
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
	return gulp.src('app/assets/scripts/*.js')
		.pipe(concat('all.js'))
		.pipe(gulp.dest('dist/js'))
		.pipe(rename('main.min.js'))
		.pipe(uglify())
});

// watch files for changes and reload
gulp.task('serve', function() {
	browserSync({
		server: {
			baseDir: 'dist'
		},
		files: ["app/assets/styles/*.scss", "dist/*.html", "dist/js/*.js"]
	});
	gulp.watch('app/assets/scripts/*.js', ['lint', 'scripts']);
	gulp.watch('app/assets/styles/*.scss', ['sass']);

});



// Watch Files For Changes
gulp.task('watch', function() {
	gulp.watch('app/assets/scripts/js/*.js', ['lint', 'scripts']);
	gulp.watch('app/assets/styles/*.scss', ['sass']);
});

// Default Task
gulp.task('default', ['lint', 'sass', ,'sprites', 'scripts', 'watch', 'browser-sync'], function () {
		gulp.watch("app/assets/styles/*.scss", ['sass']);
		gulp.watch('app/assets/scripts/js/*.js', ['lint', 'scripts']);
});

